exports.getPriceAmount =  function(item) {
	return item.price.toString().split(".")[0];
};

exports.getPriceDecimals =  function(item) {
	return item.price.toString().split(".")[1];
};